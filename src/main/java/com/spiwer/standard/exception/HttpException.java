/*
 * Copyright 2021 spiwer - Herman Leonardo Rey Baquero.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spiwer.standard.exception;

import com.spiwer.standard.template.IGenericMessage;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public class HttpException extends Exception implements GenericException
{

  private int code = -1;
  private Throwable exception;

  /**
   * Class constructor
   *
   * @param code Error code, if possible, a negative value
   * @param message Error message
   */
  public HttpException(int code, String message)
  {
    super(message);
    this.code = code;
  }

  public HttpException(Throwable ex)
  {
    super(ex.getMessage());
    this.exception = ex;
  }

  public HttpException(String message, Throwable ex)
  {
    super(message);
    this.exception = ex;
  }

  /**
   * Class constructor
   *
   * @param message Custom message
   */
  public HttpException(IGenericMessage message)
  {
    super(message.getMessage());
    this.code = message.getCode();

  }

  public void setCode(int code)
  {
    this.code = code;
  }

  @Override
  public int getCode()
  {
    return code;
  }

  public Throwable getException()
  {
    return exception;
  }

  public void setException(Throwable exception)
  {
    this.exception = exception;
  }

}
