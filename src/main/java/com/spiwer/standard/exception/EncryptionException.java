/*
 * Copyright 2022 spiwer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.spiwer.standard.exception;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public class EncryptionException extends Exception implements GenericException
{

  private int code = -1;
  private Throwable exception;

  public EncryptionException()
  {
  }

  public EncryptionException(int code, String message)
  {
    super(message);
    this.code = code;
  }

  public EncryptionException(Throwable exception)
  {
    this.exception = exception;
  }

  @Override
  public int getCode()
  {
    return code;
  }

  public Throwable getException()
  {
    return exception;
  }

  public EncryptionException setException(Throwable exception)
  {
    this.exception = exception;
    return this;
  }

}
