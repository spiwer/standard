package com.spiwer.standard.util;

import java.io.Closeable;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
@Slf4j
public class ResourceUtil
{

  public static void close(Closeable resource)
  {
    if (resource == null) {
      return;
    }
    try {
      resource.close();
    } catch (IOException ex) {
      log.error("close", ex);
    }
  }
}
