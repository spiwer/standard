package com.spiwer.standard.util;

import com.spiwer.standard.dto.HeaderInfo;
import com.spiwer.standard.template.BasicUploadFileListener;
import com.spiwer.standard.exception.HttpException;
import com.spiwer.standard.template.BasicHttpListener;
import com.spiwer.standard.template.IHttpListener;
import com.spiwer.standard.template.IJson;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
@SuppressWarnings("UseSpecificCatch")
@Slf4j
public class HttpUtil
{

  private static String encoding = "UTF-8";

  private static IJson json;

  public static IJson getJson()
  {
    if (json == null) {
      throw new RuntimeException("HttpUtil needs a configuration");
    }
    return json;
  }

  public static void setJson(IJson json)
  {
    HttpUtil.json = json;
  }

  public static String getEncoding()
  {
    return encoding;
  }

  public static void setEncoding(String encoding)
  {
    HttpUtil.encoding = encoding;
  }

  public static void post(String url, IHttpListener listener)
          throws HttpException
  {
    request(url, "POST", listener);
  }

  public static void get(String url, IHttpListener listener)
          throws HttpException
  {
    request(url, "GET", listener);
  }

  public static void put(String url, IHttpListener listener)
          throws HttpException
  {
    request(url, "PUT", listener);
  }

  public static void delete(String url, IHttpListener listener)
          throws HttpException
  {
    request(url, "DELETE", listener);
  }

  public static void request(String path, String method, IHttpListener listener)
          throws HttpException
  {
    try {
      if ("GET".equalsIgnoreCase(method)) {
        String params = listener.params();
        path += DataUtil.isFull(params) ? "?" + params : "";
      }
      if (path.startsWith("https:")) {
        executeRequestHttps(path, method, listener);
        return;
      }
      executeRequestHttp(path, method, listener);
    } catch (IOException ex) {
      log.error("request", ex);
      throw new HttpException(ex);
    }
  }

  private static void executeRequestHttps(String path, String method, IHttpListener listener)
          throws IOException, HttpException
  {
    HttpsURLConnection cnn = null;
    try {
      log.info("URL", path, "Method:", method);
      URL url = new URL(path);
      cnn = (HttpsURLConnection) url.openConnection();
      cnn.setRequestMethod(method.toUpperCase());
      cnn.setDoInput(true);
      cnn.setDoOutput(true);
      listener.requestHeaders(cnn);
      writeParams(cnn, listener);
      if (cnn.getResponseCode() >= 400) {
        listener.error(cnn, null);
      }
      readResponse(cnn, listener);
    } catch (HttpException ex) {
      throw ex;
    } catch (Throwable ex) {
      log.error("executeRequestHttps:", ex);
      listener.error(cnn, ex);
    } finally {
      if (cnn != null) {
        listener.responseHeaders(cnn.getHeaderFields());
        cnn.disconnect();
      }
    }
  }

  private static void executeRequestHttp(String path, String method, IHttpListener listener)
          throws IOException, HttpException
  {
    HttpURLConnection cnn = null;
    try {
      log.info("URL", path, "Method:", method);
      URL url = new URL(path);
      cnn = (HttpURLConnection) url.openConnection();
      cnn.setRequestMethod(method.toUpperCase());
      cnn.setDoInput(true);
      cnn.setDoOutput(true);
      listener.requestHeaders(cnn);
      writeParams(cnn, listener);
      if (cnn.getResponseCode() >= 400) {
        listener.error(cnn, null);
      }
      readResponse(cnn, listener);
    } catch (HttpException ex) {
      throw ex;
    } catch (Throwable ex) {
      log.error("executeRequestHttp:", ex);
      listener.error(cnn, ex);
    } finally {
      if (cnn != null) {
        listener.responseHeaders(cnn.getHeaderFields());
        cnn.disconnect();
      }
    }
  }

  private static void writeParams(HttpURLConnection cnn, IHttpListener listener)
          throws IOException
  {
    if ("GET".equalsIgnoreCase(cnn.getRequestMethod()) || !listener.hasInfo()) {
      return;
    }
    PrintStream out = new PrintStream(cnn.getOutputStream());
    try {
      listener.params(out);
      QueryParams params = new QueryParams();
      boolean continueParams = listener.params(params);
      if (params.getCount() != 0 && continueParams) {
        out.write(params.build().getBytes(encoding));
      }
      String strParams = listener.params();
      if (strParams != null) {
        log.info("writeParams", params);
        out.write(strParams.getBytes(encoding));
      }
      out.flush();
    } finally {
      out.close();
    }
  }

  private static void readResponse(HttpURLConnection cnn, IHttpListener listener)
          throws IOException
  {
    InputStream in = cnn.getInputStream();
    try {
      boolean response = listener.response(in);
      if (!response) {
        return;
      }
      readResponseText(in, listener);
    } finally {
      in.close();
    }
  }

  private static void readResponseText(InputStream in, IHttpListener listener)
          throws IOException
  {
    BufferedReader reader = new BufferedReader(new InputStreamReader(in, encoding));
    try {
      String line = reader.readLine();
      StringBuilder content = new StringBuilder();
      while (line != null) {
        content.append(line);
        line = reader.readLine();
      }
      listener.response(content);
    } finally {
      reader.close();
    }
  }

  public static String readResponseText(InputStream in)
          throws IOException
  {
    BufferedReader reader = new BufferedReader(new InputStreamReader(in, encoding));
    StringBuilder content = new StringBuilder();
    try {
      String line = reader.readLine();
      while (line != null) {
        content.append(line);
        line = reader.readLine();
      }
      return content.toString();
    } finally {
      reader.close();
    }
  }

  public static void sendFile(String url,
          final BasicUploadFileListener listener)
          throws HttpException
  {
    post(url, listener);
  }

  public static byte[] downloadGet(final String url)
          throws HttpException

  {
    return downloadGet(url, null, null, null);
  }

  public static byte[] downloadGet(final String url, HeaderInfo infoHeaders)
          throws HttpException

  {
    return downloadGet(url, infoHeaders, null, null);
  }

  public static byte[] downloadGet(final String url, Map<String, List<String>> headersResponse)
          throws HttpException

  {
    return downloadGet(url, null, null, headersResponse);
  }

  public static byte[] downloadGet(final String url, HeaderInfo infoHeaders, Map<String, List<String>> headersResponse)
          throws HttpException

  {
    return downloadGet(url, infoHeaders, null, headersResponse);
  }

  public static byte[] downloadGet(final String url, Object params)
          throws HttpException

  {
    return downloadGet(url, null, params, null);
  }

  public static byte[] downloadGet(final String url, HeaderInfo infoHeaders, Object params)
          throws HttpException

  {
    return downloadGet(url, infoHeaders, params, null);
  }

  public static byte[] downloadGet(final String url, Object params, Map<String, List<String>> headersResponse)
          throws HttpException

  {
    return downloadGet(url, null, params, headersResponse);
  }

  public static byte[] downloadGet(final String url,
          final HeaderInfo infoHeaders,
          final Object params,
          final Map<String, List<String>> headersResponse)
          throws HttpException

  {
    return downloadRequest(url, "GET", infoHeaders, params, headersResponse);
  }

  public static byte[] downloadRequest(final String url,
          final String method,
          final HeaderInfo infoHeaders,
          final Object params,
          final Map<String, List<String>> headersResponse)
          throws HttpException

  {
    final ByteArrayOutputStream out = new ByteArrayOutputStream();
    try {

      HttpUtil.get(url, new BasicHttpListener()
      {
        @Override
        public boolean hasInfo()
        {
          return params != null;
        }

        @Override
        public void requestHeaders(HttpURLConnection cnn)
                throws IOException
        {
          if (infoHeaders == null) {
            return;
          }
          Set<String> listNames = infoHeaders.getHeaders().keySet();
          for (String name : listNames) {
            cnn.setRequestProperty(name, infoHeaders.getHeaders().get(name));
          }
        }

        @Override
        public String params()
        {
          if (params == null) {
            return null;
          }
          if (params instanceof QueryParams) {
            return ((QueryParams) params).build();
          }
          return getJson().toJson(params);
        }

        @Override
        public boolean response(InputStream in)
                throws IOException
        {
          byte[] buffer = new byte[1024];
          int readByte;
          while ((readByte = in.read(buffer)) != -1) {
            out.write(buffer, 0, readByte);
          }
          out.flush();
          return false;
        }

        @Override
        public void responseHeaders(Map<String, List<String>> headers)
                throws IOException
        {
          if (headersResponse == null) {
            return;
          }
          headersResponse.putAll(headers);
        }

      });
      byte[] data =  out.toByteArray();
      out.close();
      return data;
    } catch (Exception ex) {
      log.error("downloadRequest", ex);
      throw new HttpException(ex);
    } finally {
      ResourceUtil.close(out);
    }
  }

  public static byte[] downloadPost(final String url)
          throws HttpException

  {
    return downloadPost(url, null, null, null);
  }

  public static byte[] downloadPost(final String url, HeaderInfo infoHeaders)
          throws HttpException

  {
    return downloadPost(url, infoHeaders, null, null);
  }

  public static byte[] downloadPost(final String url, Map<String, List<String>> headersResponse)
          throws HttpException

  {
    return downloadPost(url, null, null, headersResponse);
  }

  public static byte[] downloadPost(final String url, HeaderInfo infoHeaders, Map<String, List<String>> headersResponse)
          throws HttpException

  {
    return downloadPost(url, infoHeaders, null, headersResponse);
  }

  public static byte[] downloadPost(final String url, Object params)
          throws HttpException

  {
    return downloadPost(url, null, params, null);
  }

  public static byte[] downloadPost(final String url, HeaderInfo infoHeaders, Object params)
          throws HttpException

  {
    return downloadPost(url, infoHeaders, params, null);
  }

  public static byte[] downloadPost(final String url, Object params, Map<String, List<String>> headersResponse)
          throws HttpException

  {
    return downloadPost(url, null, params, headersResponse);
  }

  public static byte[] downloadPost(final String url,
          final HeaderInfo infoHeaders,
          final Object params,
          final Map<String, List<String>> headersResponse)
          throws HttpException

  {
    return downloadRequest(url, "POST", infoHeaders, params, headersResponse);
  }

  public static String sendFile(String url,
          Map<String, File> file)
          throws HttpException
  {
    return sendFile(url, file, null, null, null);
  }

  public static String sendFile(String url,
          Map<String, File> file,
          Map<String, Object> params)
          throws HttpException
  {
    return sendFile(url, file, null, params, null);
  }

  public static String sendFile(String url,
          Map<String, File> file,
          Map<String, Object> params,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return sendFile(url, file, null, params, headersResponse);
  }

  public static String sendFile(String url,
          Map<String, File> file,
          HeaderInfo infoHeaders)
          throws HttpException
  {
    return sendFile(url, file, infoHeaders, null, null);
  }

  public static String sendFile(String url,
          Map<String, File> file,
          HeaderInfo infoHeaders,
          Map<String, Object> params)
          throws HttpException
  {
    return sendFile(url, file, infoHeaders, params, null);
  }

  public static String sendFile(String url,
          Map<String, File> file,
          final HeaderInfo infoHeaders,
          Map<String, Object> params,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    final StringBuilder content = new StringBuilder();
    BasicUploadFileListener upload = new BasicUploadFileListener(params, file)
    {
      @Override
      public void requestHeaders(HttpURLConnection cnn)
              throws IOException
      {
        if (infoHeaders == null) {
          return;
        }
        Set<String> listNames = infoHeaders.getHeaders().keySet();
        for (String name : listNames) {
          cnn.setRequestProperty(name, infoHeaders.getHeaders().get(name));
        }
      }

      @Override
      public void response(StringBuilder response)
              throws IOException
      {
        content.append(response);
      }

      @Override
      public void responseHeaders(Map<String, List<String>> headers)
              throws IOException
      {
        if (headersResponse == null) {
          return;
        }
        headersResponse.putAll(headers);
      }

    };
    HttpUtil.sendFile(url, upload);
    return content.toString();
  }

}
