package com.spiwer.standard.util;

import com.spiwer.standard.exception.EncryptionException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
@SuppressWarnings("UseSpecificCatch")
@Slf4j
public class EncryptionUtil
{

  private String key;
  private String initVector;
  private static final String UTF8 = StandardCharsets.UTF_8.displayName();

  private EncryptionUtil()
  {
  }

  private EncryptionUtil(String key, String initVector)
  {
    this.key = key;
    this.initVector = initVector;
  }

  public static EncryptionUtil init()
  {
    return new EncryptionUtil();
  }

  public static EncryptionUtil init(String key, String initVector)
  {
    return new EncryptionUtil(key, initVector);
  }

  public String encrypt(String value)
          throws EncryptionException
  {
    try {
      IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
      SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

      byte[] encrypted = cipher.doFinal(value.getBytes());
      return Base64.getEncoder().encodeToString(encrypted);
    } catch (Exception ex) {
      log.error("encrypt", ex);
      throw new EncryptionException(ex);
    }
  }

  public String decrypt(String encrypted)
          throws EncryptionException
  {
    try {
      IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
      SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
      cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
      byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));

      return new String(original);
    } catch (Exception ex) {
      log.error("decrypt", ex);
      throw new EncryptionException(ex);
    }
  }

  public String sha256(String text)
          throws EncryptionException
  {
    return encryptText("SHA-256", text);
  }

  public String md5(String text)
          throws EncryptionException
  {
    return encryptText("MD5", text);
  }

  private String encryptText(String algorithm, String text)
          throws EncryptionException
  {

    try {
      MessageDigest digest = MessageDigest.getInstance(algorithm);
      byte[] textEncrypt = digest.digest(text.getBytes(UTF8));
      StringBuilder textHexadecimal = new StringBuilder();
      for (byte b : textEncrypt) {
        textHexadecimal.append(String.format("%02X", b));
      }
      return textHexadecimal.toString();
    } catch (Exception ex) {
      log.error("encryptText", ex);
      throw new EncryptionException(ex);
    }
  }

}
