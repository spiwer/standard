package com.spiwer.standard.util;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public class NameUtil
{

  public static String getObjectName(String name)
  {
    name = name.toLowerCase();
    String[] parts = name.split("_");
    String nameObject = parts[0];
    for (int i = 1; i < parts.length; i++) {
      nameObject += capitalize(parts[i]);
    }
    return nameObject;
  }

  private static String capitalize(String name)
  {
    return name.substring(0, 1).toUpperCase() + name.substring(1);
  }

}
