package com.spiwer.standard.util;

import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
@Slf4j
public class DataUtil
{

  private static final String REGEX_EMAIL = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$";

  private DataUtil()
  {
  }

  public static String valueOrDefault(String value, String strDefault)
  {
    return isEmpty(value) ? strDefault : value;
  }

  public static boolean isEmpty(String value)
  {
    return (value == null || value.trim().isEmpty());
  }

  public static boolean isFull(String value)
  {
    return !isEmpty(value);
  }

  public static boolean isFull(Object value)
  {
    return !isEmpty(value);
  }

  @SuppressWarnings("UseSpecificCatch")
  public static boolean isNumber(String value)
  {
    try {
      Double.valueOf(value);
      return true;
    } catch (Exception e) {
      log.info("isNumber:", e.getMessage());
      return false;
    }
  }

  @SuppressWarnings("rawtypes")
  public static boolean isEmpty(Object value)
  {
    if (value == null) {
      return true;
    }
    if (value instanceof String) {
      return isEmpty(value.toString());
    }
    if (value instanceof List) {
      return ((List) value).isEmpty();
    }
    return false;
  }

  public boolean isEmail(String email)
  {
    return email.matches(REGEX_EMAIL);
  }

}
