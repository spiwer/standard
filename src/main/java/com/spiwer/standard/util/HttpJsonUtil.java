package com.spiwer.standard.util;

import com.spiwer.standard.dto.HeaderInfo;
import com.spiwer.standard.exception.HttpException;
import com.spiwer.standard.template.BasicHttpListener;
import com.spiwer.standard.template.IJson;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
@SuppressWarnings("UseSpecificCatch")
@Slf4j
public class HttpJsonUtil
{

  public static <T> T post(String url, Type typeResponse)
          throws HttpException
  {
    return post(url, null, null, typeResponse, null);
  }

  public static <T> T post(String url, HeaderInfo infoHeaders, Type typeResponse)
          throws HttpException
  {
    return post(url, infoHeaders, null, typeResponse, null);
  }

  public static <T> T post(String url,
          Type typeResponse,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return post(url, null, null, typeResponse, headersResponse);
  }

  public static <T> T post(String url,
          HeaderInfo infoHeaders,
          Type typeResponse,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return post(url, infoHeaders, null, typeResponse, headersResponse);
  }

  public static <T> T post(String url, Object params, Type typeResponse)
          throws HttpException
  {
    return post(url, null, params, typeResponse, null);
  }

  public static <T> T post(String url, HeaderInfo infoHeaders, Object params, Type typeResponse)
          throws HttpException
  {
    return post(url, infoHeaders, params, typeResponse, null);
  }

  public static <T> T post(String url, Object params, Type typeResponse, Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return post(url, null, params, typeResponse, headersResponse);
  }

  public static <T> T post(String url,
          final HeaderInfo infoHeaders,
          final Object params,
          final Type typeResponse,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return request(url, "POST", infoHeaders, params, typeResponse, headersResponse);
  }

  private static <T> T request(String url, final String method,
          final HeaderInfo infoHeaders,
          final Object params,
          final Type typeResponse,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    try {
      final StringBuilder content = new StringBuilder();
      HttpUtil.request(url, method, new BasicHttpListener()
      {
        @Override
        public boolean hasInfo()
        {
          return params != null;
        }

        @Override
        public void requestHeaders(HttpURLConnection cnn)
                throws IOException
        {
          cnn.setRequestProperty("Content-Type", "application/json");
          if (infoHeaders == null) {
            return;
          }
          Set<String> listNames = infoHeaders.getHeaders().keySet();
          for (String name : listNames) {
            cnn.setRequestProperty(name, infoHeaders.getHeaders().get(name));
          }
        }

        @Override
        public String params()
        {
          if (params == null) {
            return null;
          }
          if ("GET".equalsIgnoreCase(method)) {
            StringBuilder infoParams = createParamsGet(params);
            return infoParams
                    .deleteCharAt(infoParams.length() - 1)
                    .toString();
          }
          return HttpUtil.getJson().toJson(params);
        }

        @Override
        public void response(StringBuilder response)
                throws IOException
        {
          content.append(response);
        }

        @Override
        public void responseHeaders(Map<String, List<String>> headers)
                throws IOException
        {
          if (headersResponse == null) {
            return;
          }
          headersResponse.putAll(headers);
        }
      });
      String response = content.toString();
      log.info("request", url, response);
      return HttpUtil.getJson().fromJson(response, typeResponse);
    } catch (Exception ex) {
      log.error("request", ex);
      throw new HttpException(ex);
    }
  }

  private static StringBuilder createParamsGet(Object params)
  {
    try {
      Map<String, ?> paramsMap = processParamGet(params);
      StringBuilder infoParams = new StringBuilder();
      Set<String> keySet = paramsMap.keySet();
      for (String key : keySet) {
        Object value = paramsMap.get(key);
        String newValue = String.valueOf(value);
        infoParams.append(key)
                .append("=")
                .append(URLEncoder.encode(newValue, "UTF-8"))
                .append("&");
      }
      return infoParams;
    } catch (Exception e) {
      log.error("createParamGet", e);
      return new StringBuilder();
    }
  }

  private static Map<String, ?> processParamGet(Object params)
  {
    if (params instanceof Map) {
      return (Map<String, ?>) params;
    }
    IJson jsonUtil = HttpUtil.getJson();
    String json = jsonUtil.toJson(params);
    Type type = jsonUtil.getMapType();
    return jsonUtil.fromJson(json, type);
  }

  public static <T> T get(String url, Type typeResponse)
          throws HttpException
  {
    return get(url, null, null, typeResponse, null);
  }

  public static <T> T get(String url, HeaderInfo infoHeaders, Type typeResponse)
          throws HttpException
  {
    return get(url, infoHeaders, null, typeResponse, null);
  }

  public static <T> T get(String url,
          Type typeResponse,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return get(url, null, null, typeResponse, headersResponse);
  }

  public static <T> T get(String url,
          HeaderInfo infoHeaders,
          Type typeResponse,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return get(url, infoHeaders, null, typeResponse, headersResponse);
  }

  public static <T> T get(String url, Object params, Type typeResponse)
          throws HttpException
  {
    return get(url, null, params, typeResponse, null);
  }

  public static <T> T get(String url, HeaderInfo infoHeaders, Object params, Type typeResponse)
          throws HttpException
  {
    return get(url, infoHeaders, params, typeResponse, null);
  }

  public static <T> T get(String url, Object params, Type typeResponse, Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return get(url, null, params, typeResponse, headersResponse);
  }

  public static <T> T get(String url,
          final HeaderInfo infoHeaders,
          final Object params,
          final Type typeResponse,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return request(url, "GET", infoHeaders, params, typeResponse, headersResponse);
  }

  public static <T> T put(String url, Type typeResponse)
          throws HttpException
  {
    return put(url, null, null, typeResponse, null);
  }

  public static <T> T put(String url, HeaderInfo infoHeaders, Type typeResponse)
          throws HttpException
  {
    return put(url, infoHeaders, null, typeResponse, null);
  }

  public static <T> T put(String url,
          Type typeResponse,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return put(url, null, null, typeResponse, headersResponse);
  }

  public static <T> T put(String url,
          HeaderInfo infoHeaders,
          Type typeResponse,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return put(url, infoHeaders, null, typeResponse, headersResponse);
  }

  public static <T> T put(String url, Object params, Type typeResponse)
          throws HttpException
  {
    return put(url, null, params, typeResponse, null);
  }

  public static <T> T put(String url, HeaderInfo infoHeaders, Object params, Type typeResponse)
          throws HttpException
  {
    return put(url, infoHeaders, params, typeResponse, null);
  }

  public static <T> T put(String url, Object params, Type typeResponse, Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return put(url, null, params, typeResponse, headersResponse);
  }

  public static <T> T put(String url,
          final HeaderInfo infoHeaders,
          final Object params,
          final Type typeResponse,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return request(url, "PUT", infoHeaders, params, typeResponse, headersResponse);
  }

  public static <T> T delete(String url, Type typeResponse)
          throws HttpException
  {
    return delete(url, null, null, typeResponse, null);
  }

  public static <T> T delete(String url, HeaderInfo infoHeaders, Type typeResponse)
          throws HttpException
  {
    return delete(url, infoHeaders, null, typeResponse, null);
  }

  public static <T> T delete(String url,
          Type typeResponse,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return delete(url, null, null, typeResponse, headersResponse);
  }

  public static <T> T delete(String url,
          HeaderInfo infoHeaders,
          Type typeResponse,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return delete(url, infoHeaders, null, typeResponse, headersResponse);
  }

  public static <T> T delete(String url, Object params, Type typeResponse)
          throws HttpException
  {
    return delete(url, null, params, typeResponse, null);
  }

  public static <T> T delete(String url, HeaderInfo infoHeaders, Object params, Type typeResponse)
          throws HttpException
  {
    return delete(url, infoHeaders, params, typeResponse, null);
  }

  public static <T> T delete(String url, Object params, Type typeResponse, Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return delete(url, null, params, typeResponse, headersResponse);
  }

  public static <T> T delete(String url,
          final HeaderInfo infoHeaders,
          final Object params,
          final Type typeResponse,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return request(url, "DELETE", infoHeaders, params, typeResponse, headersResponse);
  }

}
