package com.spiwer.standard.util;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public class QueryParams
{

  private final StringBuilder params = new StringBuilder();
  private int count = 0;

  public QueryParams add(String name, String value)
  {
    params.append(name)
            .append("=")
            .append(value)
            .append("&");
    count++;
    return this;
  }

  public String build()
  {
    return params.toString();
  }

  public int getCount()
  {
    return count;
  }

}
