package com.spiwer.standard.util;

import com.spiwer.standard.lasting.EContentType;
import com.spiwer.standard.lasting.ETypeErrorEmail;
import com.spiwer.standard.template.BasicEmailListener;
import com.spiwer.standard.template.EmailListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.activation.FileDataSource;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import jakarta.mail.util.ByteArrayDataSource;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
@SuppressWarnings("UseSpecificCatch")
@Slf4j
public final class EmailUtil
{

  private Properties properties;
  private EmailListener listener;
  private final Map<String, String> listTo = new HashMap<String, String>();
  private final Map<String, String> listCc = new HashMap<String, String>();
  private final Map<String, String> listBcc = new HashMap<String, String>();
  private final List<File> listAttachments = new ArrayList<File>();
  private final Map<String, String> listAttachmentsBase64 = new HashMap<String, String>();
  private final Map<String, File> listImages = new HashMap<String, File>();
  private final Map<String, String> listImagesString = new HashMap<String, String>();
  private Session session;
  private Transport transport;
  private boolean reportError = true;
  private String fromEmail;
  private String subject;
  private String content;
  private File template;
  private EContentType contentType = EContentType.TEXT_PLAIN;
  private final Multipart multipart = new MimeMultipart();
  private MimeMessage message;
  private String user;
  private String password;
  private final String protocol = "smtp";
  private Boolean massive = Boolean.FALSE;
  private int counter = 0;

  private EmailUtil(Properties properties)
  {
    this.properties = properties;
  }

  private EmailUtil(Properties properties, EmailListener listener)
  {
    this(properties);
    this.listener = listener;

  }

  /**
   * <a
   * href=https://javaee.github.io/javamail/docs/api/com/sun/mail/smtp/package-summary.html>
   * https://javaee.github.io/javamail/docs/api/com/sun/mail/smtp/package-summary.html
   * </a>
   *
   * @param properties Configuration
   * @return Object of the class
   */
  public static EmailUtil init(Properties properties)
  {
    return new EmailUtil(properties);
  }

  public static EmailUtil init(Properties properties, EmailListener listener)
  {
    return new EmailUtil(properties, listener);
  }

  public EmailUtil setReportError(boolean reportError)
  {
    this.reportError = reportError;
    return this;
  }

  public EmailUtil addTo(String email)
  {
    listTo.put(email, "");
    return this;
  }

  public EmailUtil addTo(String email, String name)
  {
    listTo.put(email, name);
    return this;
  }

  public EmailUtil addCc(String email, String name)
  {
    listCc.put(email, name);
    return this;
  }

  public EmailUtil addBcc(String email, String name)
  {
    listBcc.put(email, name);
    return this;
  }

  public EmailUtil addAttachment(File file)
  {
    listAttachments.add(file);
    return this;
  }

  public EmailUtil addAttachmentBase64(String name, String file)
  {
    listAttachmentsBase64.put(listAttachmentsBase64.size() + "-" + name, file);
    return this;
  }

  public EmailUtil addImageTemplate(String cid, File file)
  {
    listImages.put(cid, file);
    return this;
  }

  public EmailUtil addBase64Image(String cid, String contentFile)
  {
    listImagesString.put(cid, contentFile);
    return this;
  }

  public EmailUtil setContentType(EContentType type)
  {
    this.contentType = type;
    return this;
  }

  public EmailUtil setContent(String content)
  {
    if (template == null) {
      contentType = EContentType.TEXT_PLAIN;
    }
    this.content = content;
    return this;
  }

  public EmailUtil setContentTemplate(File template)
  {
    contentType = EContentType.TEXT_HTML;
    this.template = template;
    return this;
  }

  public EmailUtil setContentTemplate(String template)
  {
    contentType = EContentType.TEXT_HTML;
    this.content = template;
    return this;
  }

  public EmailUtil setSubject(String subject)
  {
    this.subject = subject;
    return this;
  }

  public EmailUtil setFromEmail(String fromEmail)
  {
    this.fromEmail = fromEmail;
    return this;
  }

  public EmailUtil setMassive(Boolean massive)
  {
    this.massive = massive;
    return this;
  }

  public EmailUtil setListener(EmailListener listener)
  {
    this.listener = listener;
    return this;
  }

  private void connect()
  {
    try {
      if (transport != null && transport.isConnected()) {
        return;
      }
      transport = session.getTransport(protocol);
      if ("true".equalsIgnoreCase(properties.get("mail.smtp.auth").toString())) {
        transport.connect(user, password);
        return;
      }
      transport.connect();
    } catch (MessagingException ex) {
      log.error("connect", ex);
      error(ex, ETypeErrorEmail.CONNECT);
    }
  }

  private void initParameters()
  {
    if (listener == null) {
      listener = new BasicEmailListener();
    }
    session = Session.getDefaultInstance(properties);
    user = properties.getProperty("mail.smtp.user");
    password = properties.getProperty("mail.smtp.password");
    properties.remove("mail.smtp.user");
    properties.remove("mail.smtp.password");
    listener.connect(properties);
  }

  private void error(Throwable ex, ETypeErrorEmail type)
  {
    if (reportError) {
      throw new RuntimeException(ex);
    }
    if (listener != null) {
      listener.error(ex, type);
    }
  }

  public void send()
  {
    initParameters();
    processContent();
    String newContent = listener.emailContent(this.content);
    this.content = newContent;
    message = new MimeMessage(session);
    processImagesTemplate();
    processImagesStringTemplate();
    processAttachment();
    processAttachmentBase64();
    setSubject();
    setTextContent();
    setFrom();
    addEmails(listTo, Message.RecipientType.TO, message);
    addEmails(listCc, Message.RecipientType.CC, message);
    addEmails(listBcc, Message.RecipientType.BCC, message);
    setContentEmail();
    sendMessage();
    disconnect();
  }

  private void disconnect()
  {
    if (massive && counter <= 50) {
      return;
    }
    try {
      transport.close();
      transport = null;
      counter = 0;
    } catch (MessagingException ex) {
      log.error("disconnect", ex);
    }
  }

  private void setContentEmail()
  {
    try {
      message.setContent(multipart);
    } catch (MessagingException ex) {
      error(ex, ETypeErrorEmail.CONTENT);
    }
  }

  private void sendMessage()
  {
    try {
      connect();
      log.info("Connected with server");
      transport.sendMessage(message, message.getAllRecipients());
    } catch (MessagingException ex) {
      error(ex, ETypeErrorEmail.SEND);
    }
    counter++;
  }

  private void setFrom()
  {
    try {
      if (fromEmail == null) {
        fromEmail = user;
      }
      message.setFrom(new InternetAddress(fromEmail));
    } catch (MessagingException ex) {
      error(ex, ETypeErrorEmail.FROM);
    }
  }

  private void setSubject()
  {
    try {
      message.setSubject(subject);
    } catch (MessagingException ex) {
      error(ex, ETypeErrorEmail.SUBJECT);
    }
  }

  private void setTextContent()
  {
    try {
      MimeBodyPart body = new MimeBodyPart();
      body.setContent(content, contentType.getType());
      multipart.addBodyPart(body);
    } catch (MessagingException ex) {
      error(ex, ETypeErrorEmail.CONTENT);
    }
  }

  private void addEmails(Map<String, String> emails, Message.RecipientType type, MimeMessage message)
  {
    Set<String> listKey = emails.keySet();
    for (String email : listKey) {
      try {
        String name = emails.get(email);
        message.addRecipient(type, new InternetAddress(email, name));
      } catch (UnsupportedEncodingException ex) {
        error(ex, ETypeErrorEmail.RECIPIENT);
      } catch (MessagingException ex) {
        error(ex, ETypeErrorEmail.RECIPIENT);
      }
    }
  }

  private void processContent()
  {
    if (this.template == null || !this.template.exists()) {
      log.info("template email: {0}", template);
      return;
    }
    this.content = this.content == null ? "" : this.content;
    StringBuilder contentTemplate = new StringBuilder(this.content);
    BufferedReader reader = null;
    try {
      reader = new BufferedReader(
              new InputStreamReader(new FileInputStream(this.template), Charset.forName("UTF-8")));
      String line = reader.readLine();
      while (line != null) {
        contentTemplate.append(line);
        line = reader.readLine();
      }
    } catch (IOException ex) {
      log.error("processContent", ex);
      error(ex, ETypeErrorEmail.TEMPLATE);
    } finally {
      ResourceUtil.close(reader);
    }

    this.content = contentTemplate.toString();
    log.info("Info template finished ");
  }

  private void processAttachment()

  {
    for (File attachment : listAttachments) {
      try {
        MimeBodyPart part = new MimeBodyPart();
        part.attachFile(attachment);
        multipart.addBodyPart(part);
      } catch (IOException ex) {
        error(ex, ETypeErrorEmail.ATTACHMENT);
      } catch (MessagingException ex) {
        error(ex, ETypeErrorEmail.ATTACHMENT);
      }
    }
  }

  private void processAttachmentBase64()

  {
    Set<String> keys = listAttachmentsBase64.keySet();
    for (String name : keys) {
      try {
        String fileData = listAttachmentsBase64.get(name);
        byte[] fileBytes = Base64.getDecoder().decode(fileData);
        DataSource dataSource = new ByteArrayDataSource(fileBytes, "application/octet-stream");
        MimeBodyPart part = new MimeBodyPart();
        part.setDataHandler(new DataHandler(dataSource));
        part.setFileName(name);
        multipart.addBodyPart(part);
      } catch (Exception ex) {
        error(ex, ETypeErrorEmail.ATTACHMENT);
      }
    }
  }

  private void processImagesTemplate()
  {
    File file;
    MimeBodyPart part;
    Set<String> listNameImages = listImages.keySet();
    for (String cid : listNameImages) {
      try {
        file = listImages.get(cid);
        part = new MimeBodyPart();
        part.setHeader("Content-ID", "<" + cid + ">");
        part.setDisposition(MimeBodyPart.INLINE);
        part.setDataHandler(new DataHandler(new FileDataSource(file)));
        multipart.addBodyPart(part);
      } catch (Exception ex) {
        error(ex, ETypeErrorEmail.IMAGES);
      }
    }
  }

  private void processImagesStringTemplate()
  {
    String contentImage;
    MimeBodyPart part;
    Set<String> listNameImages = listImagesString.keySet();
    for (String cid : listNameImages) {
      try {
        contentImage = listImagesString.get(cid);
        part = new MimeBodyPart();
        part.setHeader("Content-ID", "<" + cid + ">");
        part.setDisposition(MimeBodyPart.INLINE);
        part.setFileName(cid);
        byte[] data = Base64.getDecoder().decode(contentImage);
        ByteArrayDataSource source = new ByteArrayDataSource(data, "application/octet-stream");
        part.setDataHandler(new DataHandler(source));
        multipart.addBodyPart(part);
      } catch (Exception ex) {
        error(ex, ETypeErrorEmail.IMAGES);
      }
    }
  }
}
