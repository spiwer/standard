package com.spiwer.standard.util;

import com.spiwer.standard.dto.HeaderInfo;
import com.spiwer.standard.exception.HttpException;
import com.spiwer.standard.template.BasicHttpListener;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
@SuppressWarnings("UseSpecificCatch")
public class HttpPlainUtil
{

  public static String request(String url,
          String method,
          final HeaderInfo infoHeaders,
          final QueryParams params,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    final StringBuilder content = new StringBuilder();
    HttpUtil.request(url, method, new BasicHttpListener()
    {
      @Override
      public boolean hasInfo()
      {
        return params != null;
      }

      @Override
      public void requestHeaders(HttpURLConnection cnn)
              throws IOException
      {
        if (infoHeaders == null) {
          return;
        }
        Set<String> listNames = infoHeaders.getHeaders().keySet();
        for (String name : listNames) {
          cnn.setRequestProperty(name, infoHeaders.getHeaders().get(name));
        }
      }

      @Override
      public String params()
      {
        if (params == null) {
          return null;
        }

        return params.build();

      }

      @Override
      public void response(StringBuilder response)
              throws IOException
      {
        content.append(response);
      }

      @Override
      public void responseHeaders(Map<String, List<String>> headers)
              throws IOException
      {
        if (headersResponse == null) {
          return;
        }
        headersResponse.putAll(headers);
      }

    });
    return content.toString();

  }

  public static String post(String url)
          throws HttpException
  {
    return post(url, null, null, null);
  }

  public static String post(String url, HeaderInfo infoHeaders)
          throws HttpException
  {
    return post(url, infoHeaders, null, null);
  }

  public static String post(String url,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return post(url, null, null, headersResponse);
  }

  public static String post(String url,
          HeaderInfo infoHeaders,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return post(url, infoHeaders, null, headersResponse);
  }

  public static String post(String url, QueryParams params)
          throws HttpException
  {
    return post(url, null, params, null);
  }

  public static String post(String url, HeaderInfo infoHeaders, QueryParams params)
          throws HttpException
  {
    return post(url, null, params, null);
  }

  public static String post(String url, QueryParams params, Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return post(url, null, params, headersResponse);
  }

  public static String post(String url,
          final HeaderInfo infoHeaders,
          final QueryParams params,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return request(url, "POST", infoHeaders, params, headersResponse);
  }

  public static String get(String url)
          throws HttpException
  {
    return get(url, null, null, null);
  }

  public static String get(String url, HeaderInfo infoHeaders)
          throws HttpException
  {
    return get(url, infoHeaders, null, null);
  }

  public static String get(String url,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return get(url, null, null, headersResponse);
  }

  public static String get(String url,
          HeaderInfo infoHeaders,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return get(url, infoHeaders, null, headersResponse);
  }

  public static String get(String url, QueryParams params)
          throws HttpException
  {
    return get(url, null, params, null);
  }

  public static String get(String url, HeaderInfo infoHeaders, QueryParams params)
          throws HttpException
  {
    return get(url, null, params, null);
  }

  public static String get(String url, QueryParams params, Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return get(url, null, params, headersResponse);
  }

  public static String get(String url,
          final HeaderInfo infoHeaders,
          final QueryParams params,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return request(url, "GET", infoHeaders, params, headersResponse);

  }

  public static String put(String url)
          throws HttpException
  {
    return put(url, null, null, null);
  }

  public static String put(String url, HeaderInfo infoHeaders)
          throws HttpException
  {
    return put(url, infoHeaders, null, null);
  }

  public static String put(String url,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return put(url, null, null, headersResponse);
  }

  public static String put(String url,
          HeaderInfo infoHeaders,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return put(url, infoHeaders, null, headersResponse);
  }

  public static String put(String url, QueryParams params)
          throws HttpException
  {
    return put(url, null, params, null);
  }

  public static String put(String url, HeaderInfo infoHeaders, QueryParams params)
          throws HttpException
  {
    return put(url, null, params, null);
  }

  public static String put(String url, QueryParams params, Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return put(url, null, params, headersResponse);
  }

  public static String put(String url,
          final HeaderInfo infoHeaders,
          final QueryParams params,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return request(url, "PUT", infoHeaders, params, headersResponse);

  }

  public static String delete(String url)
          throws HttpException
  {
    return delete(url, null, null, null);
  }

  public static String delete(String url, HeaderInfo infoHeaders)
          throws HttpException
  {
    return delete(url, infoHeaders, null, null);
  }

  public static String delete(String url,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return delete(url, null, null, headersResponse);
  }

  public static String delete(String url,
          HeaderInfo infoHeaders,
          Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return delete(url, infoHeaders, null, headersResponse);
  }

  public static String delete(String url, QueryParams params)
          throws HttpException
  {
    return delete(url, null, params, null);
  }

  public static String delete(String url, HeaderInfo infoHeaders, QueryParams params)
          throws HttpException
  {
    return delete(url, null, params, null);
  }

  public static String delete(String url, QueryParams params, Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return delete(url, null, params, headersResponse);
  }

  public static String delete(String url,
          final HeaderInfo infoHeaders,
          final QueryParams params,
          final Map<String, List<String>> headersResponse)
          throws HttpException
  {
    return request(url, "DELETE", infoHeaders, params, headersResponse);

  }

}
