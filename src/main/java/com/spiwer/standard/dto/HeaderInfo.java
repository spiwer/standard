package com.spiwer.standard.dto;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public class HeaderInfo
{

  private final Map<String, String> headers = new HashMap<String, String>();

  public HeaderInfo add(String name, String value)
  {
    headers.put(name, value);
    return this;
  }

  public Map<String, String> getHeaders()
  {
    return headers;
  }

}
