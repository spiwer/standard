package com.spiwer.standard.lasting;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public enum ETypeErrorEmail
{
  FROM,
  ATTACHMENT,
  RECIPIENT,
  SEND,
  CONNECT,
  CONTENT,
  SUBJECT,
  TEMPLATE,
  IMAGES
}
