package com.spiwer.standard.lasting;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public enum EContentType
{
  TEXT_PLAIN("text/plain; charset=utf-8"),
  TEXT_HTML("text/html; charset=utf-8"),
  TEXT_XML("text/xml; charset=utf-8");
  private final String type;

  private EContentType(String type)
  {
    this.type = type;
  }

  public String getType()
  {
    return type;
  }

}
