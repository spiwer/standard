package com.spiwer.standard.column;

import com.spiwer.standard.lasting.ETypePrimaryKey;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public class PrimaryKey
{

  private final transient String columnName;
  private final transient Class classPrimaryKey;
  private final transient ETypePrimaryKey type;

  public PrimaryKey(String columnName, Class classPrimaryKey, ETypePrimaryKey type)
  {
    this.columnName = columnName;
    this.classPrimaryKey = classPrimaryKey;
    this.type = type;
  }

  public String getColumnName()
  {
    return columnName;
  }

  public ETypePrimaryKey getType()
  {
    return type;
  }

  public Class getClassPrimaryKey()
  {
    return classPrimaryKey;
  }

}
