package com.spiwer.standard.template;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public interface IGenericMessage
{

  Integer getCode();

  String getMessage();

}
