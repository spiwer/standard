package com.spiwer.standard.template;

import com.spiwer.standard.exception.HttpException;
import com.spiwer.standard.util.HttpUtil;
import com.spiwer.standard.util.QueryParams;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
@SuppressWarnings({"UseSpecificCatch", "ThrowableResultIgnored"})
@Slf4j
public class BasicHttpListener implements IHttpListener
{

  @Override
  public void requestHeaders(HttpURLConnection cnn)
          throws IOException
  {
  }

  @Override
  public String params()
  {
    return null;
  }

  @Override
  public boolean params(QueryParams params)
          throws IOException
  {
    return true;
  }

  @Override
  public void params(OutputStream out)
          throws IOException
  {
  }

  @Override
  public void response(StringBuilder response)
          throws IOException
  {
  }

  @Override
  public boolean response(InputStream in)
          throws IOException
  {
    return true;
  }

  @Override
  public void responseHeaders(Map<String, List<String>> headers)
          throws IOException
  {
  }

  @Override
  public void error(HttpURLConnection cnn, Throwable ex)
          throws HttpException
  {
    String info = null;
    try {
      log.info("Response Code:", cnn.getResponseCode(), " " + cnn.getResponseMessage());
      info = HttpUtil.readResponseText(cnn.getErrorStream());
    } catch (Exception e) {
      log.error("error", e);
    }
    throw new HttpException(info, ex);
  }

  @Override
  public boolean hasInfo()
  {
    return true;
  }

}
