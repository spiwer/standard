package com.spiwer.standard.template;

import com.spiwer.standard.util.QueryParams;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public class BasicUploadFileListener extends BasicHttpListener
{

  private final Map<String, Object> params;
  private final Map<String, File> files;
  private static final String LINE_END = "\r\n";
  private static final String TWO_HYPHENS = "--";
  private static final String BOUNDARY = "*****";

  public BasicUploadFileListener(Map<String, Object> params, Map<String, File> files)
  {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    if (files == null) {
      files = new HashMap<String, File>();
    }
    this.params = params;
    this.files = files;

  }

  @Override
  public void requestHeaders(HttpURLConnection cnn)
          throws IOException
  {
    cnn.setUseCaches(false);
    cnn.setChunkedStreamingMode(1024);
    cnn.setRequestProperty("Connection", "Keep-Alive");
    cnn.setRequestProperty("ENCTYPE", "multipart/form-data");
    cnn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);
  }

  @Override
  public void params(OutputStream out)
          throws IOException
  {
    DataOutputStream dataOut = new DataOutputStream(out);
    Set<String> listNames = params.keySet();
    for (String name : listNames) {
      Object value = params.get(name);
      if (value == null) {
        continue;
      }
      dataOut.writeBytes(TWO_HYPHENS + BOUNDARY + LINE_END);
      String header = "Content-Disposition: form-data; name=\"" + name + "\"" + LINE_END;
      dataOut.writeBytes(header);
      dataOut.writeBytes(LINE_END);
      dataOut.writeBytes(value.toString());
      dataOut.writeBytes(LINE_END);
      dataOut.writeBytes(TWO_HYPHENS + BOUNDARY + TWO_HYPHENS + LINE_END);
    }
    Set<String> listFile = files.keySet();
    for (String name : listFile) {
      File file = files.get(name);
      if (file == null) {
        continue;
      }
      dataOut.writeBytes(TWO_HYPHENS + BOUNDARY + LINE_END);
      String header = "Content-Disposition: form-data; name=\"" + name + "\";filename=\""
              + file.getName() + "\"" + LINE_END;
      dataOut.writeBytes(header);
      dataOut.writeBytes(LINE_END);
      byte[] buffer = new byte[1024];
      int readByte;
      FileInputStream in = new FileInputStream(file);
      while ((readByte = in.read(buffer)) != -1) {
        dataOut.write(buffer, 0, readByte);
      }
      in.close();
      dataOut.writeBytes(LINE_END);
      dataOut.writeBytes(TWO_HYPHENS + BOUNDARY + TWO_HYPHENS + LINE_END);
      dataOut.flush();
    }

  }

  @Override
  public final boolean params(QueryParams params)
          throws IOException
  {
    return false;
  }

  @Override
  public final String params()
  {
    return null;
  }

  @Override
  public boolean hasInfo()
  {
    return true;
  }

}
