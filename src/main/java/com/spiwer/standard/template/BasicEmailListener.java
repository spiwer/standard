package com.spiwer.standard.template;

import com.spiwer.standard.lasting.ETypeErrorEmail;

import java.util.Properties;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
@Slf4j
public class BasicEmailListener implements EmailListener
{

  @Override
  public String emailContent(String emailContent)
  {
    return emailContent;
  }

  @Override
  public void error(Throwable ex, ETypeErrorEmail type)
  {
    log.error("BasicEmailListener", ex);
  }

  @Override
  public void connect(Properties properties)
  {
    log.info("connect", properties);
  }

}
