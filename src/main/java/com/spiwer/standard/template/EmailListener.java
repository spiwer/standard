package com.spiwer.standard.template;

import com.spiwer.standard.lasting.ETypeErrorEmail;
import java.util.Properties;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public interface EmailListener
{

  public abstract String emailContent(String emailContent);

  public abstract void error(Throwable ex, ETypeErrorEmail type);

  public abstract void connect(Properties properties);

}
