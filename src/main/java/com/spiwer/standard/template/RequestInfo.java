package com.spiwer.standard.template;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public interface RequestInfo
{

  public abstract <T extends Object> T infoSession();

  public abstract String getIdLog();

}
