package com.spiwer.standard.template;

import com.spiwer.standard.exception.HttpException;
import com.spiwer.standard.util.QueryParams;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

/**
 *
 * @author spiwer.com - Herman Leonardo Rey Baquero - leoreyb@gmail.com
 */
public interface IHttpListener
{

  public void requestHeaders(HttpURLConnection cnn)
          throws IOException;

  public boolean hasInfo();

  public String params();

  public void params(OutputStream out)
          throws IOException;

  public boolean params(QueryParams params)
          throws IOException;

  public void response(StringBuilder response)
          throws IOException;

  public boolean response(InputStream in)
          throws IOException;

  public void responseHeaders(Map<String, List<String>> headers)
          throws IOException;

  public void error(HttpURLConnection cnn, Throwable ex)
          throws HttpException;

}
